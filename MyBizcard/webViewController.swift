//
//  webViewController.swift
//  MyBizcard
//
//  Created by iMac on 27/04/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class webViewController: UIViewController, UIWebViewDelegate {
    
    //webView
    @IBOutlet weak var webViewCnt: UIWebView!
    //signup url
    let signupUrl = URL(string: "http://sportal.dev.weplatform.co.kr:19990/comm_0015_01.act?PREV_CB_URL=login_0001_01.act")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         webViewCnt.delegate = self
        
       if let urls = signupUrl{
            let req = URLRequest(url: urls)
            webViewCnt.loadRequest(req)
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


