//
//  loginViewController.swift
//  MyBizcard
//
//  Created by iMac on 24/04/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class loginViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var txtUserId: UITextField!
    @IBOutlet weak var txtUserPwd: UITextField!
    @IBOutlet weak var ImgRemember: UIImageView!
    @IBOutlet weak var chkbox_off_icon: UIImageView!
    
  //  let encodeURL = jSonData.jsonAPI.jsonDataValue.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    var mess: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let UITapRecognizerChk_on = UITapGestureRecognizer(target: self, action: #selector(tappedImage))
        UITapRecognizerChk_on.delegate = self
        self.ImgRemember.addGestureRecognizer(UITapRecognizerChk_on)
        self.ImgRemember.isUserInteractionEnabled = true
        
        let UITapRecognizerChk_off = UITapGestureRecognizer(target: self, action: #selector(tappedImageOff))
        UITapRecognizerChk_off.delegate = self
        self.chkbox_off_icon.addGestureRecognizer(UITapRecognizerChk_off)
        self.chkbox_off_icon.isUserInteractionEnabled = true
    }
    
    func tappedImage(){
        self.ImgRemember.isHidden = true
    }
    
    func tappedImageOff(){
        self.ImgRemember.isHidden = false
    }
    
    //MARK: - Login Func
    func LoginUser (usrId: String, usrPwd: String){
        
        let postString = "{\"CNTS_CRTS_KEY\":\"\",\"TRAN_NO\":\"MYCD_MBL_P001\",\"REQ_DATA\":{\"USER_ID\":\"\(usrId)\",\"USER_PW\":\"\(usrPwd)\"}}".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let str = "\(jSonData.jsonAPI.APIBase)\(jSonData.jsonAPI.jsonDataKey)\(postString!)"
        var reqURL = URLRequest(url: URL(string: str)!)
        reqURL.httpMethod = "POST"
        reqURL.httpBody = postString?.data(using: .utf8)
        let tasks = URLSession.shared.dataTask(with: reqURL, completionHandler: {(data, response, error) in
            if error != nil{
                return
            }else{
                let jsons = try? JSONSerialization.jsonObject(with: data!, options: [])
                
                
                if let dictionary = jsons as? [String : Any]{
                    
                    if let RSLT_CD = dictionary["RSLT_CD"] as? String{
                        if let RSLT_MSG = dictionary["RSLT_MSG"] as? String{
                            if RSLT_CD == "0000" {
                                
                                let alertController = UIAlertController(title: "로그인 되었습니다" , message: RSLT_MSG , preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "확인", style: .default, handler: nil))
                                DispatchQueue.main.async {
                          
                                    self.present(alertController, animated: true, completion: nil)
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainStrory")
                                    appDelegate.window?.rootViewController = initialViewController
                                    appDelegate.window?.makeKeyAndVisible()
                                    
                                    let vc = initialViewController as! MainViewController
                                    if let RESP_DATA = dictionary["RESP_DATA"] as? [String : Any]{
                                        let userName = RESP_DATA["USER_NM"] as! String
                                        let dept = RESP_DATA["BSNN_NM"] as! String
                                      
                                        vc.txtUsrName.text = userName
                                        vc.txtDept.text = dept
                                    }
                                    
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.alertMsg(title: RSLT_CD , message: RSLT_MSG)
                                    
                                }
                                
                            }
                        }
                    }
                    
                    //{"USER_ID":"woojin8321t01","USER_PW":"qwer1234!"}}
                }
            
            }
        
        })
        
    tasks.resume()
    }
    
    //MARK: - Login Button Action
    
    public func alertMsg (title: String , message: String){
        let alertController = UIAlertController(title: title , message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "확인", style: .cancel, handler: nil))
        DispatchQueue.main.async {
            self.present(alertController, animated: false, completion: nil)
        }
    }
    
    
    @IBAction func btnLogin(_ sender: UIButton) {
        let usrId = txtUserId.text!
        let usrPwd = txtUserPwd.text!
        
                let dataRow = JsonDataInit(usrId: usrId, usrPw: usrPwd)
                LoginUser(usrId: dataRow.usrId, usrPwd: dataRow.usrPw)
        
    }
    
    //MARK: - Remember Method
    


    // MARK: - Navigation


 
}
