//
//  MainViewController.swift
//  MyBizcard
//
//  Created by iMac on 24/04/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var txtUsrName: UILabel!
    @IBOutlet weak var txtDept: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
 
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BtnLogOut(_ sender: UIButton) {
        print("Login Out")
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let initialViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginMain")
            appDelegate.window?.rootViewController = initialViewController
            appDelegate.window?.makeKeyAndVisible()
            
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
