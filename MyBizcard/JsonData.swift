//
//  JsonData.swift
//  MyBizcard
//
//  Created by iMac on 24/04/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(_ updates: @escaping() -> Void){
    DispatchQueue.main.sync {
        updates()
    }
}

struct jSonData {
    //MARK: - WebserVice API
    struct jsonAPI {
        
        static let APIBase : String = "http://172.20.50.239:28080/CardAPI.do?"
        static let jsonDataKey : String = "JSONData="
       // static let jsonDataValue  = "{\"CNTS_CRTS_KEY\":\"\",\"TRAN_NO\":\"MYCD_MBL_P001\",\"REQ_DATA\":{\"USER_ID\":\"\",\"USER_PW\":\"\"}}"
        
    }
}



//static let jsonDataValue = "{\"CNTS_CRTS_KEY\":\"\",\"TRAN_NO\":\"MYCD_MBL_P001\",\"REQ_DATA\":{\"\(usrIdKey)\":\"\(usrIdValue)\",\"\(usrPwdKey)\":\"\(usrPwdValue)!\"}}"
//static let usrIdKey = "USER_ID"
//static let usrIdValue = "woojin8321t01"
//static let usrPwdKey = "USER_PW"
//static let usrPwdValue = "qwer1234"

//Post
class JsonDataInit{
    let usrId : String
    let usrPw : String
    
    init(usrId:String, usrPw:String) {
        self.usrId = usrId
        self.usrPw = usrPw
    }
}

class ResponseData{
    let name : String
    let dept : String
    
    init(name:String, dept:String) {
        self.name = name
        self.dept = dept
    }
}
