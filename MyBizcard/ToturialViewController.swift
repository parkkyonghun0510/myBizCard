//
//  ToturialViewController.swift
//  MyBizcard
//
//  Created by iMac on 25/04/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class ToturialViewController: UIPageViewController
{
 //Some hard data for coding 
    var pageHeader = ["속쉬운 경비처리", "사진촬영 첨보", "사용금액, 미처리 영수증", "비필 법인카드"]
    var pageDescreption1 = ["영수증에 용도와 메모만 입력하여", "카드영수증에 실몰영수증", "이번달 사용금액과 미처리 앵수증을", "언제 어디서나 쉽고 빠른 경비처리"]
    var pageDescreption2 = ["영수증을 보내면 끝!", "또는 증빙자료를 첨보하세요", "한눈에 확인하여 놓치는 일이 없습니다", "지금 시작하세요!"]
    var pageImage = ["intro_img_01", "intro_img_02", "intro_img_03",""]
    
    //Make status bar white(light Content)
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //this class is page viewcontroller data source itself
        self.dataSource = self
        
        //create the first Tutorial VC
        if let startTutorialVC = self.viewControllerIndx(0) {
            setViewControllers([startTutorialVC], direction: .forward, animated: true, completion: nil)
        }
        
    }
    
    //MARK: Navigate
    func nextPageWithIndx(_ index : Int) {
        if let nextTutorialVC = self.viewControllerIndx(index+1){
            setViewControllers([nextTutorialVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func viewControllerIndx(_ index : Int) -> ViewController? {
        if index == NSNotFound || index < 0 || index >= self.pageDescreption1.count {
            return nil
        }
        //create a new Tutorials viewcontroller and assng appropreate date
        
        if let TutorialViewController = storyboard?.instantiateViewController(withIdentifier: "toturial_a") as? ViewController {
            TutorialViewController.headerText = pageHeader[index]
            TutorialViewController.description1 = pageDescreption1[index]
            TutorialViewController.description2 = pageDescreption2[index]
            TutorialViewController.imageName    = pageImage[index]
            TutorialViewController.index    = index
            
            return TutorialViewController
        }
        return nil
    }
    
}

extension ToturialViewController : UIPageViewControllerDataSource
{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ViewController).index
        index += 1
        return self.viewControllerIndx(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! ViewController).index
        index -= 1
        return self.viewControllerIndx(index)
    }
}
