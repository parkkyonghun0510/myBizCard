//
//  FogotIdViewController.swift
//  MyBizcard
//
//  Created by iMac on 01/05/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class FogotIdViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webViewId: UIWebView!
    
    
    //ForgetId load url
    let forgetId = URL(string: "http://sportal.dev.weplatform.co.kr:19990/comm_0022_01.act")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webViewId.delegate = self
        
//        if let urls = forgetId{
//            let req = URLRequest(url: urls)
//            webViewId.loadRequest(req)
//            print("\(webViewId.loadRequest(req))")
//            
//        }
        
        let myURLString = "http://sportal.dev.weplatform.co.kr:19990/comm_0022_01.act"
        guard let myURL = URL(string: myURLString) else {
            print("Error: \(myURLString) doesn't seem to be a valid URL")
            return
        }
        
        do {
            let myHTMLString = try String(contentsOf: myURL, encoding: .utf8)
            
            webViewId.loadHTMLString(myHTMLString, baseURL: myURL)
            print("HTML : \(myHTMLString)")
        } catch let error {
            print("Error: \(error)")
        }

  
    }
    
    
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//    let path = Bundle.main.path(forResource: "styles", ofType: "css")
//    let javaScriptStr = "var link = document.createElement('link'); link.href = '%@'; link.rel = 'stylesheet'; document.head.appendChild(link)"
//    let javaScripthPath = NSString(format: javaScriptStr as NSString, path!)
//    webView.stringByEvaluatingJavaScript(from: javaScripthPath as String)
//    
//    //Second way
//    do {
//    let cssContent = try String(contentsOf: , encoding: <#T##String.Encoding#>)
//    let javaScrStr = "var style = document.createElement('style'); style.innerHTML = '%@'; document.head.appendChild(style)"
//    let JavaScrWithCSS = NSString(format: javaScrStr, cssContent)
//    self.articleView.stringByEvaluatingJavaScriptFromString(JavaScrWithCSS as String)
//    
//    }
//    catch let error as NSError {
//    print(error);
//    }
//    }

    @IBAction func BackToLogin(_ sender: UIButton) {
        DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
