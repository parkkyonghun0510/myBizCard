//
//  ForgotPwViewController.swift
//  MyBizcard
//
//  Created by iMac on 01/05/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class ForgotPwViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet weak var webview: UIWebView!

    //signup url
    let forgetId = URL(string: "http://sportal.dev.weplatform.co.kr:19990/comm_0022_01.act")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webview.delegate = self
        
        if let urls = forgetId{
            let req = URLRequest(url: urls)
            webview.loadRequest(req)
            
        }
    }
    
    @IBAction func BackButtonPW(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
