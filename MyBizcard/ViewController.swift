//
//  ViewController.swift
//  MyBizcard
//
//  Created by iMac on 24/04/2017.
//  Copyright © 2017 KOSIGN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblDescreption1: UILabel!
    @IBOutlet weak var lblDescreption2: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageControll: UIPageControl!
    
    //webview
    
    
    //MARK: - Data model for each tutorail screen
    var index = 0
    var headerText = ""
    var imageName = ""
    var description1 = ""
    var description2 = ""
    
    //bar status whit bar lightContent 
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    //1.let config a tutorial screen with data model
    override func viewDidLoad() {
        super.viewDidLoad()
            lblHeader.text = headerText
            lblDescreption1.text = description1
            lblDescreption2.text = description2
            imageView.image = UIImage(named: imageName)
            pageControll.currentPage = index
        
        // cutomize the sho login button
        if index == 3{
              labels()
        }
    }
    
    //2. if the user click on loginButton we will dismiss
    @IBAction func loginButton(_ sender: AnyObject) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "GoLogin")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func labels(){
        
//        let widthConstraint = NSLayoutConstraint(item: lblHeader, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 300)
        let heightConstraint = NSLayoutConstraint(item: lblHeader, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 37)
        var constraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:[superview]-(<=1)-[label]",
            options: NSLayoutFormatOptions.alignAllCenterX,
            metrics: nil,
            views: ["superview":view, "label":lblHeader])
        
        view.addConstraints(constraints)
        
        // Center vertically
        constraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:[superview]-(<=1)-[label]",
            options: NSLayoutFormatOptions.alignAllCenterY,
            metrics: nil,
            views: ["superview":view, "label":lblHeader])
        
        view.addConstraints(constraints)
        
        view.addConstraints([heightConstraint])
    }
    

}

